<?php
namespace Drupal\custom_db_table_views\Controller;
use Drupal\Core\Controller\ControllerBase;

/**
 * An settings example controller.
 */
class SettingsCustomController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
	global $base_path;
    $build = array(
      '#type' => 'markup',
      '#markup' => '<img src="'.$base_path.'modules/custom_db_table_views/image/settings_example.jpg">',
    );
    $build = ['#type' => 'markup', '#markup' => 'Latest Feature coming soon....'];
    return $build;
  }
 
}
